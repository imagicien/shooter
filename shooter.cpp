using namespace std;

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>

//#include <SDL2/SDL.h>
//#include <SDL2/SDL_image.h>
#include "SDL.h"
#include "SDL_image.h"

//#include <stdlib.h>     /* srand, rand */
//#include <time.h>       /* time */

#include "utils.h"
#include "config.h"
#include "niveaux.h"
#include "classes.h"

int main(int argc, char *argv[])
{
    /*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
    |	I N I T I A L I S A T I O N		|
    \*_________________________________*/

    //INIT SDL
    SDL_Init(SDL_INIT_VIDEO) == 0 or die("SDL_Init", 1);

    //Créer fenetre
    SDL_Window *win = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    win != nullptr or die("SDL_CreateWindow", 2);

    //Créer renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    renderer != nullptr or die("SDL_CreateRenderer", 3);


    /*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
    | C O N T E X T E  &  E S P A C E S |
    \*_________________________________*/

    Contexte contexte = creer_contexte();

    Espace espace(contexte, renderer);
    espace.creer_groupes();
    espace.charger_ressources();
    espace.charger_donnees();


    /*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
    |	 B O U C L E     PRINCIPALE     |
    \*_________________________________*/

    while(contexte.il_faut_rouler())
    {
        espace.gerer_input();
        espace.update_tous();
        espace.draw_tous();
    }


    /*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
    |			M É N A G E				|
    \*_________________________________*/

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(win);
    SDL_Quit();
}

Contexte creer_contexte()
{

}
