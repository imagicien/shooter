#ifndef ESPACES_H
#define ESPACES_H

#include "utils.h" //ref a SDL

using namespace std;

typedef unsigned int uint;

class Gameobject;
class Espace;
class Contexte;

class Gameobject
{
public:
    Gameobject();

    virtual void update() = 0;
    virtual void draw() = 0;
};

class Espace
{
private:
    SDL_Renderer* _renderer;

public:
    Espace(Contexte contexte, SDL_Renderer* renderer);

    //Initialisation
    virtual void creer_groupes()        = 0; // show me your pure virtual shit
    virtual void charger_ressources()   = 0;
    virtual void charger_donnees()      = 0;

    //Déroulement
    virtual void gerer_input()          = 0;
    virtual void update_tous()          = 0;
    virtual void draw_tous()            = 0;
};

class Contexte
{
private:
    bool continuer;

public:
    Contexte();

    //Initialisation

    //Déroulement
    bool il_faut_rouler() { return continuer; }
};

#endif // ESPACES_H
