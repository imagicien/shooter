#include "SDL.h"
#include "SDL_image.h"
#include <iostream>
#include <string>

void logSDLError(std::ostream &os, const std::string &msg);

bool die(std::string message, int code_erreur);

SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren);

void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y);
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, const SDL_RendererFlip flip_x);
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, double angle, SDL_Point* center, SDL_RendererFlip flip);


class Angle
{
private:
    double _angle_radians;
public:
    Angle(double angle_radians);
    Angle(double angle, bool est_degres);

    double get_rad();
    double get_deg();
    void set_rad();
    void set_deg();
};

class Transform
{
private:
    SDL_Point position;
    Angle rotation;
    double echelle_x;
    double echelle_y;
public:
    Transform();
};
