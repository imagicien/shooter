#include <iostream>
#include <string>
#include "config.h"

#include "SDL.h"
#include "SDL_image.h"

/**
* Log an SDL error with some error message to the output stream of our choice
* @param os The output stream to write the message to
* @param msg The error message to write, format will be msg error: SDL_GetError()
*/
void logSDLError(std::ostream &os, const std::string &msg){
	os << msg << " error: " << SDL_GetError() << std::endl;
}
bool die(std::string message, int code_erreur = 99)
{
	std::cout << message << " | Erreur SDL: " << SDL_GetError() << std::endl;
	//XXX quitter();
	exit(code_erreur);
	return true;
}

/**
* Loads a BMP image into a texture on the rendering device
* @param file The BMP image file to load
* @param ren The renderer to load the texture onto
* @return the loaded texture, or nullptr if something went wrong.
*/
SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren){
    SDL_Texture *texture = IMG_LoadTexture(ren, (DATA_PATH + file).c_str());
	texture != nullptr or die("LoadTexture");
	return texture;
}
	/*
	SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren){
		//Initialize to nullptr to avoid dangling pointer issues
		SDL_Texture *texture = nullptr;
		//Load the image
		SDL_Surface *loadedImage = SDL_LoadBMP(file.c_str());
		//If the loading went ok, convert to texture and return the texture
		if (loadedImage != nullptr){
			texture = SDL_CreateTextureFromSurface(ren, loadedImage);
			SDL_FreeSurface(loadedImage);
			//Make sure converting went ok too
			texture != nullptr or die("CreateTextureFromSurface");
		}
		else
			die("LoadBMP");

		return texture;
	}
	*/

/**
* Draw an SDL_Texture to an SDL_Renderer at position x, y, preserving
* the texture's width and height
* @param tex The source texture we want to draw
* @param ren The renderer we want to draw to
* @param x The x coordinate to draw to
* @param y The y coordinate to draw to
*/
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	//Query the texture to get its width and height to use
	SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h); //XXX performance?
	SDL_RenderCopy(ren, tex, NULL, &dst);
}
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, const SDL_RendererFlip flip_x){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	//Query the texture to get its width and height to use
	SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h); //XXX performance?
	SDL_RenderCopyEx(ren, tex, NULL, &dst, 0, NULL, flip_x);
}
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, double angle, SDL_Point* center, SDL_RendererFlip flip){
    //Setup the destination rectangle to be at the position we want
    SDL_Rect dst;
    dst.x = x;
    dst.y = y;
    //Query the texture to get its width and height to use
    SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h); //XXX performance?
    SDL_RenderCopyEx(ren, tex, NULL, &dst, angle, center, flip);
}

//Supprimer pointeur
//http://stackoverflow.com/questions/1361139/c-vector-of-pointers-to-objects?rq=1
/*template <typename T>
void delete_pointed_to(T* const ptr)
{
    delete ptr;
}*/
