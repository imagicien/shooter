#include "espace.h"


/*
 * GAMEOBJECT -----------------------------------------------------------
*/
Gameobject::Gameobject()
{
}

/*
 * GROUPE -----------------------------------------------------------
*/
//Groupe::Groupe() {}
Groupe::Groupe(Espace *espace, string nom)
{
    _espace = espace;
    _nom = nom;
}

//Gameobject* Groupe::creer_objet()
//{
//    return ajouter_objet( Gameobject(this) );
//}
//Gameobject* Groupe::creer_objet(string nom)
//{
//    return ajouter_objet(nom, Gameobject(this, nom));
//}

Gameobject* Groupe::ajouter_objet(Gameobject &objet)
{
    objet._groupe = this;
    objet._renderer = _espace->_renderer;
    _objets.push_back(objet);
    return &_objets.back();
}
Gameobject* Groupe::ajouter_objet(string nom, Gameobject& objet)
{
    Gameobject* objet_ptr = ajouter_objet(objet);
    _objets_par_nom[nom] = objet_ptr;
    return objet_ptr;
}
Gameobject* Groupe::get_objet(string nom)
{
    return _objets_par_nom[nom];
}
vector<Gameobject*> Groupe::get_objets()
{
    vector<Gameobject*> objets_dans_groupe{ std::begin(_objets), std::end(_objets) };
    return objets_dans_groupe;
}
void Groupe::update()
{
    for(Gameobject o : _objets)
        o.update();
}
void Groupe::draw()
{
    for(Gameobject o : _objets)
        o.draw();
}

/*
 * ESPACE -----------------------------------------------------------
*/
Espace::Espace() {}
Espace::Espace(string nom)
{
    _nom = nom;
}
Groupe* Espace::creer_groupe(string nom)
{
    _groupes[nom] = Groupe(this, nom);
    return &_groupes[nom];
}
Groupe* Espace::get_groupe(string nom)
{
    return &_groupes[nom];
}
vector<Gameobject*> Espace::get_objets()
{
    vector<Gameobject*> objets_dans_espace;
    vector<Gameobject*> objets_dans_groupe;
    vector<Gameobject*>::iterator i;
    for(Groupe g : _groupes)
    {
        objets_dans_groupe = g.get_objets();

        i = objets_dans_espace.end(); //inserer a partir de
        objets_dans_espace.reserve(objets_dans_espace.size() + objets_dans_groupe.size());
        objets_dans_espace.insert(i, objets_dans_groupe.begin(), objets_dans_groupe.end());
    }
    return objets_dans_espace;
}
void Espace::update()
{
    for(Groupe g : _groupes)
        g.update();
}
void Espace::draw()
{
    for(Groupe g : _groupes)
        g.draw();
}
