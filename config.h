#include "SDL.h"

/////////////////////////////////////////////////
// UTILS

const std::string DATA_PATH = "../data/";
const double DEG_TO_RAD = 0.01745329251994;
const double RAD_TO_DEG = 57.2957795130823;

/////////////////////////////////////////////////
// TEMPS

const Uint32 FPS = 60; // Hz
const Uint32 FPS_T = 1000/FPS; // ms


/////////////////////////////////////////////////
// AFFICHAGE

const int SCREEN_WIDTH  = 1024;
const int SCREEN_HEIGHT = 768;


/////////////////////////////////////////////////
// MAP

const int CASE_W = 64;
const int CASE_H = 64;

/////////////////////////////////////////////////
// JOUEUR

const int JOUEUR_W = 32;
const int JOUEUR_H = 32;
const int JOUEUR_POS_INIT_X = SCREEN_WIDTH/2;
const int JOUEUR_POS_INIT_Y = SCREEN_HEIGHT - 2*CASE_H;
//point JOUEUR_POS_INIT;
//JOUEUR_POS_INIT.x = JOUEUR_POS_INIT_X;
//JOUEUR_POS_INIT.y = JOUEUR_POS_INIT_Y;
const int SCROLL_VITESSE = 4; // px/frame
const int JOUEUR_V_X = 8;
const double JOUEUR_V_ROT = 3; // deg/frame
const int JOUEUR_BOOST = 3;
const std::string JOUEUR_IMAGE = "vaisseau3.png";

/////////////////////////////////////////////////
// PROJECTILES

const int PROJ_W = 16;
const int PROJ_H = 32;
const int PROJ_V = 16;
const int PROJ_DOMMAGE = 1;
const std::string PROJ_IMAGE = "balle.png";

/////////////////////////////////////////////////
// ROCHES

const int ROCHE_VIE = 3;
const std::string ROCHE_MORTE_IMAGE = "roche_morte_box.png";
const std::string ROCHE_PASMORTE_IMAGE = "roche_pasmorte_box.png";

/////////////////////////////////////////////////
// DRONE

const int DRONE_X = 128; //SCREEN_WIDTH/2;
const int DRONE_Y = 128;
const int DRONE_W = 128;
const int DRONE_H = 64;
const int DRONE_VIE = 10;
const std::string DRONE_IMAGE = "drone.png";
const int DRONE_V_X = 3;
const int DRONE_T_TIR_MS = 1200; // periode tir (ms)
const int DRONE_T_TIR = DRONE_T_TIR_MS/FPS; //periode tir (frame)
const int DRONE_CANON_Y = DRONE_H/2 + PROJ_H/2 + 1;

