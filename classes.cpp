#include <vector>
#include <list>
#include "utils.h"
#include "config.h"
#include "classes.h"
#include "espace.h"

/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
|			J O U E U R				|
\*_________________________________*/

Joueur::Joueur()
{
	tex = loadTexture(JOUEUR_IMAGE, ren);
	rect.w = JOUEUR_W;
    rect.h = JOUEUR_H;
    angle = 90;
}
Joueur::~Joueur()
{
	SDL_DestroyTexture(tex);
}
void Joueur::update()
{
    rect.x = pos_x - JOUEUR_W;
    rect.y = pos_y - JOUEUR_H;
}
void Joueur::draw()
{
    renderTexture(tex, _renderer, JOUEUR_POS_INIT_X, JOUEUR_POS_INIT_Y, 0, NULL, SDL_FLIP_NONE);
}
//void Joueur::update_draw()
//{
//    update();
//    draw();
//}
void Joueur::placer(int p_x, int p_y)
{
	pos_x = p_x;
	pos_y = p_y;
	rect.x = pos_x - JOUEUR_W/2; //XXX superflu
	rect.y = pos_y - JOUEUR_H/2;
}
void Joueur::move(int dx, int dy)
{
	pos_x += dx;
	pos_y += dy;
	rect.x = pos_x - JOUEUR_W/2; //XXX superflu
	rect.y = pos_y - JOUEUR_H/2;
}
void Joueur::tourner(double dtheta)
{
    angle += dtheta;
    if(angle < 0) angle += 360;
}
void Joueur::avancer(double distance)
{
    distance = -distance;
    pos_x += distance * cos(angle*DEG_TO_RAD);
    pos_y += distance * sin(angle*DEG_TO_RAD);
}
void Joueur::camera_shift_future_system(double &pos_x_obj, double &pos_y_obj)
{
    double dx = pos_x_obj - pos_x;
    double dy = pos_y_obj - pos_y;
    double distance_obj = sqrt( dx*dx + dy*dy );
    double angle_obj = atan2(dy,dx); //radians

    angle_obj -= (angle - 90)*DEG_TO_RAD;
    pos_x_obj = JOUEUR_POS_INIT_X + distance_obj * cos(angle_obj);
    pos_y_obj = JOUEUR_POS_INIT_Y + distance_obj * sin(angle_obj);
}

/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
|			R O C H E				|
\*_________________________________*/

Roche::Roche(int *scroll_ptr, int posx, int posy)
{
	scroll = scroll_ptr;
	
	pos_x = posx;
	pos_y = posy;
	
	rect.x = pos_x - CASE_W/2; 
	rect.y = pos_y - CASE_H/2;
	rect.w = CASE_W;
	rect.h = CASE_H;
	
	existe = true;
	pts_vie = ROCHE_VIE;
	
	modif_couleur = 255; //XXX
}
Roche::~Roche()
{
	SDL_DestroyTexture(tex_morte);
	SDL_DestroyTexture(tex_pasmorte);
}

void Roche::update(Joueur& joueur)
{
    if(!existe) return;

    double pos_x_shift = pos_x-CASE_W/2;
    double pos_y_shift = pos_y-CASE_H/2;
    joueur.camera_shift_future_system(pos_x_shift, pos_y_shift);
    rect.x = pos_x_shift;
    rect.y = pos_y_shift;
}
void Roche::draw()
{
    if(!existe) return;

    SDL_Texture *tex = cassable ? tex_morte : tex_pasmorte;

    SDL_SetTextureColorMod(tex_morte, 255, modif_couleur, modif_couleur); //XXX
    renderTexture(tex, _renderer, rect.x, rect.y, -(joueur.angle - 90), NULL, SDL_FLIP_NONE);
    SDL_SetTextureColorMod(tex_morte, 255, 255, 255); //XXX

    if(modif_couleur < 255) modif_couleur += 10; //XXX revenir couleur normale
}
//void Roche::update_draw(Joueur& joueur)
//{
//    if(existe)
//	{
//        double pos_x_shift = pos_x-CASE_W/2;
//        double pos_y_shift = pos_y-CASE_H/2;
//        joueur.camera_shift_future_system(pos_x_shift, pos_y_shift);
//        rect.x = pos_x_shift;
//        rect.y = pos_y_shift;
		
//        SDL_Texture *tex = cassable ? tex_morte : tex_pasmorte;
		
//        SDL_SetTextureColorMod(tex_morte, 255, modif_couleur, modif_couleur); //XXX
//        renderTexture(tex, ren, rect.x, rect.y, -(joueur.angle - 90), NULL, SDL_FLIP_NONE);
//        SDL_SetTextureColorMod(tex_morte, 255, 255, 255); //XXX
		
//		if(modif_couleur < 255) modif_couleur += 10; //XXX revenir couleur normale
//	}
//}
void Roche::brise()
{
	if(cassable)
	{
		pts_vie -= PROJ_DOMMAGE;
		if(pts_vie == 0) existe = false;
		
		modif_couleur = 105; //XXX
	}
}

/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
|		P R O J E C T I L E			|
\*_________________________________*/

Projectile::Projectile(int posx, int posy, double v_y, double nouv_angle, bool est_allie)
{
	pos_x = posx;
	pos_y = posy;
	rect.x = pos_x - PROJ_W/2; //XXX superflu
	rect.y = pos_y - PROJ_H/2;
	rect.w = PROJ_W;
	rect.h = PROJ_H;
	existe = true;
	vitesse = v_y;

    angle = nouv_angle < 360 ? (nouv_angle >= 0 ? nouv_angle : nouv_angle + 360) : nouv_angle - 360;
    angle_depart = nouv_angle;

	allie = est_allie;
}
Projectile::~Projectile()
{
	SDL_DestroyTexture(tex);
}
void Projectile::update(Joueur &joueur)
{
    if(!existe) return;

    pos_x += -vitesse * cos(angle*DEG_TO_RAD);
    pos_y += -vitesse * sin(angle*DEG_TO_RAD);

    //rect.x = pos_x - PROJ_W/2;
    //rect.y = pos_y - PROJ_H/2;
    //shift camera
    double pos_x_shift = pos_x;
    double pos_y_shift = pos_y;
    joueur.camera_shift_future_system(pos_x_shift, pos_y_shift);
    rect.x = pos_x_shift + PROJ_W/2;
    rect.y = pos_y_shift - PROJ_H/2;
}

void Projectile::draw(Joueur &joueur)
{
    if(existe) return;

    renderTexture(tex, _renderer, rect.x, rect.y, -(joueur.angle - angle_depart), NULL, SDL_FLIP_NONE);
}
void Projectile::kill()
{
	existe = false;
}


/*¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯*\
|			D R O N E				|
\*_________________________________*/

Drone::Drone(std::list<Projectile*> *proj, int posx, int posy)
{
	projectiles = proj;
	
	pos_x = posx;
	pos_y = posy;
	
	rect.x = pos_x - DRONE_W/2; 
	rect.y = pos_y - DRONE_H/2;
	rect.w = DRONE_W;
	rect.h = DRONE_H;
	
	existe = true;
	pts_vie = DRONE_VIE;
	
	modif_couleur = 255; //XXX
	mouv_x = DRONE_V_X;
	attendre_tir = DRONE_T_TIR;
}
Drone::~Drone()
{
	SDL_DestroyTexture(tex);
}
void Drone::update(Joueur &joueur)
{
    //Bouger
    double dx = joueur.pos_x - pos_x;
    double dy = joueur.pos_y - pos_y;
    double distance_joueur = sqrt( dx*dx + dy*dy );
    double angle_vers_joueur = atan2(dy,dx)*RAD_TO_DEG;

    angle = angle_vers_joueur;
    if( distance_joueur > 300 )
        avancer(4);
    else if( distance_joueur < 200 )
        avancer(-3);

    //shift camera
    double pos_x_shift = pos_x;
    double pos_y_shift = pos_y;
    joueur.camera_shift_future_system(pos_x_shift, pos_y_shift);
    rect.x = pos_x_shift - DRONE_W/2;
    rect.y = pos_y_shift - DRONE_H/2;

    //Tirer
    if(attendre_tir == 0)
    {
        tirer();
        attendre_tir = DRONE_T_TIR;
    }
    else
        attendre_tir--;
}
void Drone::draw()
{
    double dx = rect.x - JOUEUR_POS_INIT_X;
    double dy = rect.y - JOUEUR_POS_INIT_Y;
    double angle_vers_joueur_ecran = atan2(dy,dx)*RAD_TO_DEG;

    SDL_SetTextureColorMod(tex, 255, modif_couleur, modif_couleur); //XXX
    renderTexture(tex, _renderer, rect.x, rect.y, angle_vers_joueur_ecran + 90, NULL, SDL_FLIP_NONE);
    SDL_SetTextureColorMod(tex, 255, 255, 255); //XXX

    if(modif_couleur < 255) modif_couleur += 10; //XXX revenir couleur normale
}
void Drone::avancer(double distance)
{
    pos_x += distance * cos(angle*DEG_TO_RAD);
    pos_y += distance * sin(angle*DEG_TO_RAD);
}

void Drone::brise()
{
	pts_vie -= PROJ_DOMMAGE;
    //if(pts_vie == 0) existe = false;
    if(pts_vie == 0) YEAH();
	
	modif_couleur = 105; //XXX
}
void Drone::tirer()
{
    //projectiles->push_back( new Projectile(ren, rect.x, pos_y + DRONE_CANON_Y, -PROJ_V, false) );
    //projectiles->push_back( new Projectile(ren, rect.x + DRONE_W, pos_y + DRONE_CANON_Y, -PROJ_V, false) );
    projectiles->push_back( new Projectile(ren, pos_x, pos_y, PROJ_V, angle+180, false) );
}
void Drone::YEAH()
{
	existe = true;
	pts_vie = DRONE_VIE;
    pos_x = DRONE_X;
    pos_y = DRONE_Y;
}
