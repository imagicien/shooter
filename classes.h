#include <vector>
#include <list>
#include "SDL.h"
#include "SDL_image.h"
#include "utils.h"
#include "espace.h"

class Joueur : public Gameobject
{
public:
    double pos_x;
    double pos_y;
    double angle;
	SDL_Rect rect;
	SDL_Texture *tex;
	
    Joueur();
	~Joueur();
    void update();
    void draw();
	void update_draw();
	void placer(int p_x, int p_y);
	void move(int dx, int dy);
    void tourner(double dtheta);
    void avancer(double distance);
    void camera_shift_future_system(double &pos_x, double &pos_y);
};

class Roche : public Gameobject
{
public:
    double pos_x;
    double pos_y;
	SDL_Rect rect;
	bool cassable;
	bool existe;
	int *scroll;
	static SDL_Texture *tex_morte;
	static SDL_Texture *tex_pasmorte;
	int pts_vie;
	int modif_couleur; //XXX
	
	Roche();
    Roche(int *scroll_ptr, int posx, int posy);
	~Roche();
    void update(Joueur &joueur);
    void draw(Joueur &joueur);
    void update_draw(Joueur&);
	void brise();
};

class Projectile : public Gameobject
{
public:
	int pos_x;
	int pos_y;
    double vitesse;
    double angle;
    double angle_depart;
	bool existe;
	SDL_Rect rect;
	static SDL_Texture *tex;
	bool allie; //appartient au joueur
	
	Projectile();
    Projectile(int posx, int posy, double v_y, double angle, bool est_allie);
	~Projectile();
    void update(Joueur& joueur);
    void draw(Joueur& joueur);
    void update_draw(Joueur &joueur);
	void kill();
};

class Drone : public Gameobject
{
public:
    double pos_x;
    double pos_y;
    double angle;
	SDL_Rect rect;
	bool existe;
	static SDL_Texture *tex;
	int pts_vie;
	int modif_couleur; //XXX
	int mouv_x;
	int attendre_tir;
    std::list<Projectile*> *projectiles; //par pointeur (ARK ARK) XXX
	
	Drone();
    Drone(std::list<Projectile*> *proj, int posx, int posy);
	~Drone();
    void update(Joueur &joueur);
    void draw();
    //void update_draw(Joueur &joueur);
    void avancer(double distance);
	void brise();
	void tirer();
	void YEAH();
};
